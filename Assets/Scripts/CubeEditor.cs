﻿using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(Waypoint))]
public class CubeEditor : MonoBehaviour {
    
    private TextMesh textMesh;
    private Waypoint waypoint;
    private int gridSize;

    private void Awake() {
        waypoint = GetComponent<Waypoint>();
        textMesh = GetComponentInChildren<TextMesh>();
        gridSize = Waypoint.GetGridSize();
    }

    private void Update() {
        var gridPos = waypoint.GridPos;
        SnapToGrid(gridPos);
        FormatLabel(gridPos);
    }

    private void SnapToGrid(Vector3Int gridPos) {
        transform.position = new Vector3(gridPos.x * gridSize, 0f, gridPos.z * gridSize);
    }

    private void FormatLabel(Vector3Int gridPos) {
        var labelText = string.Format("{0}, {1}", gridPos.x, gridPos.z);
        textMesh.text = labelText;
        gameObject.name = string.Format("Cube ({0})", labelText);
    }
}