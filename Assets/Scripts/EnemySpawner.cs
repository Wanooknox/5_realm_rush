﻿using System.Collections;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    [SerializeField]
    [Range(0.1f, 120f)]
    private float timeBetweenSpawns = 3f;

    [SerializeField]
    private EnemyMovement enemyPrefab;

    void Start() {
        StartCoroutine(SpawnEnemy());
    }

    private IEnumerator SpawnEnemy() {
        while (true) {
            Instantiate(enemyPrefab);
            yield return new WaitForSeconds(timeBetweenSpawns);
        }
    }
}