﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour {
    [SerializeField]
    private float nodeWaitTime = 2f;

    private void Start() {
        Pathfinder pathfinder = FindObjectOfType<Pathfinder>();
        var path = pathfinder.Path;
        StartCoroutine(FollowPath(path));
    }

    private IEnumerator FollowPath(List<Waypoint> path) {
        foreach (var waypoint in path) {
            this.transform.position = waypoint.transform.position;
            nodeWaitTime = 2f;
            yield return new WaitForSeconds(nodeWaitTime);
        }
    }
}