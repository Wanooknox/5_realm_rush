﻿using UnityEngine;

public class Waypoint : MonoBehaviour {
    private const int GRID_SIZE = 10;

    public bool IsExplored = false;
    public bool IsObstructed = false;
    public Waypoint QueuedBy;

    public Vector3Int GridPos {
        get {
            return new Vector3Int {
                x = Mathf.RoundToInt(transform.position.x / GRID_SIZE),
                y = 0,
                z = Mathf.RoundToInt(transform.position.z / GRID_SIZE)
            };
        }
    }

    public static int GetGridSize() {
        return GRID_SIZE;
    }

    public void SetTopColor(Color color) {
        MeshRenderer topMeshRenderer = transform.Find("Top").GetComponent<MeshRenderer>();
        topMeshRenderer.material.color = color;
    }

    private void OnMouseOver() {
        if (Input.GetMouseButtonUp(0) && !IsObstructed) {
            print(string.Format("Clicked block: {0}", gameObject.name));
        }
    }
}