﻿using System.Linq;
using UnityEngine;

public class Tower : MonoBehaviour {
    // Params
    [SerializeField]
    private float attackRange = 10f;

    [SerializeField]
    private ParticleSystem bullets;

    [SerializeField]
    private Transform objectToPan;

    // State
    private Transform targetEnemy;

    private void Update() {
        FindTargetEnemy();
        // doing this this way is pretty manual. Could use collider and collision event
        if (CheckEnemyExists() && CheckEnemyInRange()) {
            TargetEnemy();
            EnableShooting(true);
        } else {
            EnableShooting(false);
        }
    }

    private void FindTargetEnemy() {
        var sceneEnemies = FindObjectsOfType<EnemyDamage>();
        if (!sceneEnemies.Any()) {
            return;
        }

        Transform closestEnemy = sceneEnemies[0].transform;
        foreach (EnemyDamage enemy in sceneEnemies) {
            closestEnemy = GetClosest(closestEnemy, enemy.transform);
        }

        targetEnemy = closestEnemy;
    }

    private Transform GetClosest(Transform transformA, Transform transformB) {
        var distanceA = Vector3.Distance(transform.position, transformA.position);
        var distanceB = Vector3.Distance(transform.position, transformB.position);
        return (distanceA > distanceB) ? transformB : transformA;
    }

    private bool CheckEnemyInRange() {
        return Vector3.Distance(targetEnemy.position, transform.position) <= attackRange;
    }

    private bool CheckEnemyExists() {
        return targetEnemy != null;
    }

    private void TargetEnemy() {
        objectToPan.LookAt(targetEnemy.position);
    }

    private void EnableShooting(bool shouldShoot) {
        var bulletsEmission = bullets.emission;
        bulletsEmission.enabled = shouldShoot;
    }
}