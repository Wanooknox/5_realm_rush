﻿using UnityEngine;

public class EnemyDamage : MonoBehaviour {
	
	[SerializeField]
	private int hitPoints = 10;

	private void OnParticleCollision(GameObject other) {
		ProcessHit();
		if (hitPoints < 1) {
			Destroy(gameObject);
		}
	}

	void ProcessHit() {
		hitPoints -= 1;
	}
}
