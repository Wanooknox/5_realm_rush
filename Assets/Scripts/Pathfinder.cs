﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pathfinder : MonoBehaviour {
    private List<Waypoint> path = new List<Waypoint>();

    [SerializeField]
    private Waypoint startWaypoint;

    [SerializeField]
    private Waypoint endWaypoint;

    private Dictionary<Vector3Int, Waypoint> grid = new Dictionary<Vector3Int, Waypoint>();

    private Queue<Waypoint> queue = new Queue<Waypoint>();

    private bool isRunning = true;

    private Vector3Int[] directions = {
        new Vector3Int(0, 0, 1),
        Vector3Int.right,
        new Vector3Int(0, 0, -1),
        Vector3Int.left,
    };

    public List<Waypoint> Path {
        get {
            if (!path.Any()) {
                LoadBlocks();
                Pathfind_BFS();
                BuildPath();
            }
            return path;
        }
    }

    private void BuildPath() {
        AddToPath(endWaypoint);
        
        Waypoint previous = endWaypoint.QueuedBy;
        while (previous != startWaypoint) {
            AddToPath(previous);
            previous = previous.QueuedBy;
        }

        AddToPath(startWaypoint);
        path.Reverse();
    }

    private void AddToPath(Waypoint waypoint) {
        path.Add(waypoint);
        waypoint.IsObstructed = true;
    }

    private void Pathfind_BFS() {
        startWaypoint.SetTopColor(Color.green);
        queue.Enqueue(startWaypoint);

        while (queue.Count > 0 && isRunning) {
            var currWaypoint = queue.Dequeue();
            currWaypoint.IsExplored = true;
            HaltOnEndFound(currWaypoint);
            ExploreNeighbours(currWaypoint);
        }
    }

    private void HaltOnEndFound(Waypoint searchCenter) {
        if (searchCenter == endWaypoint) {
            endWaypoint.SetTopColor(Color.red);
            isRunning = false;
        }
    }

    private void ExploreNeighbours(Waypoint currWaypoint) {
        if (!isRunning) {
            return;
        }

        foreach (var direction in directions) {
            Vector3Int validNode = currWaypoint.GridPos + direction;

            if (grid.ContainsKey(validNode)) {
                AddNeighbourToQueue(validNode, currWaypoint);
            }
        }
    }

    private void AddNeighbourToQueue(Vector3Int validNode, Waypoint searchCenter) {
        Waypoint neighbour = grid[validNode];
        if (neighbour.IsExplored || queue.Contains(neighbour)) {
            return;
        }

        neighbour.SetTopColor(Color.yellow); //todo remove
        queue.Enqueue(neighbour);
        neighbour.QueuedBy = searchCenter; // not really EXPLORED only QUEUED for exploration
    }

    private void LoadBlocks() {
        Waypoint[] waypoints = FindObjectsOfType<Waypoint>();
        foreach (Waypoint waypoint in waypoints) {
            var gridPos = waypoint.GridPos;
            if (grid.ContainsKey(gridPos)) {
                Debug.Log("Skipping Overlapping block: " + waypoint);
            } else {
                grid.Add(gridPos, waypoint);
            }
        }

        Debug.Log(string.Format("Loaded {0} blocks", grid.Count));
    }
}